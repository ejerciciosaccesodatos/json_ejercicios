# Ejercicios JSON #

Ejercicios JSON de Juan Antonio Suarez Rosa

### ¿Qué contiene este repositorio? ###

* Ejercicio Seleccion Ciudad
* Ejercicio Tiempo 7 días
* Ejercicio Cambio Divisas
* Ejercicio Libre, OMS

### Ejercicio  Seleccion Ciudad ###

* En este ejercicio se muestra en una ista unas ciudades que al pulsar sobre una te muestra detaladamente el tiempo de hoy de esa ciudad en forma de card
* Se usa una clase Weather para los objetos Json mostrada en el ejemplo de la API

### Ejercicio Tiempo 7 días ###

* En este ejercicio hay un edittext en el cual introducees una ciudad, y pulsando en el botón se descargarán los siguientes 7 dias del tiempo que se mostrarán en una list view
* Se guarda el objeto Json en un archivo en la memoria, se pide otra peticion a la api para el modo xml y se guarda en otro archivo prediccionesCiudad.xml y .json
* Se usa una clase Weather para los objetos Json mostrada en el ejemplo de la API

### Ejercicio Cambio Divisas ###

* Este ejercicio escoge en una web un objeto Json para conseguir el valor del cambio euro-dolar y para usarlo en el ejercicio que es igual al de la otra relación.

### Ejercicio Libre, OMS ###

* Este ejercicio utiliza los datos abiertos de la **Organización Mundial de la salud**
* Existe una *lista inicial con los 3 apartados que he utilizado para el ejercicio*. los 3 son muy similares, son datos estadísticos de diferentes paises, años y valores relacionados con.
1. Prevalencia de violencia entre parejas íntimas de mujeres
2. Hombres que tienen sexo con hombres con sífilis activa
3. Muertes a causa del tabaco

**1. Prevalencia de violencia entre parejas íntimas de mujeres (Objeto Json tipo "fact")**

```
#!json

'fact': [
           {
             "dim": {
                      "YEAR": "2010",
                      "SEX": "Female",
                      "GBDREGION": "WORLD",
                      "AGEGROUP": "15-69  (total) years",
                      "WHOINCOMEREGION": "World",
                      "GHO": "Intimate partner violence prevalence among ever partnered women (%)",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "30 [27.8-32.2]"
           },
```

- A partir de este objeto se crea esta clase para su serialización y utilización como objeto GSON, mostrandose una lista con todos los datos de los paises ordenados alfabéticamente, año y % de prevalencia.

```
#!java
public class Violence implements Serializable {
        private String YEAR;
        private String SEX;
        private String AGEGROUP;
        private String WHOINCOMEREGION;
        private String GBDREGION;
        private String GHO;

        public String getYEAR() {
            return YEAR;
        }

        public void setYEAR(String YEAR) {
            this.YEAR = YEAR;
        }

        public String getSEX() {
            return SEX;
        }

        public void setSEX(String SEX) {
            this.SEX = SEX;
        }

        public String getAGEGROUP() {
            return AGEGROUP;
        }

        public void setAGEGROUP(String AGEGROUP) {
            this.AGEGROUP = AGEGROUP;
        }

        public String getWHOINCOMEREGION() {
            return WHOINCOMEREGION;
        }

        public void setWHOINCOMEREGION(String WHOINCOMEREGION) {
            this.WHOINCOMEREGION = WHOINCOMEREGION;
        }

        public String getGBDREGION() {
            return GBDREGION;
        }

        public void setGBDREGION(String GBDREGION) {
            this.GBDREGION = GBDREGION;
        }

        public String getGHO() {
            return GHO;
        }

        public void setGHO(String GHO) {
            this.GHO = GHO;
        }
    }

```
![Screenshot_1483483225.png](https://bitbucket.org/repo/Lq7x6a/images/3423435301-Screenshot_1483483225.png)
 

**2. Hombres que tienen sexo con hombres con sífilis activa (Objeto Json tipo "fact")**

```
#!json
"fact": [
           {
             "dim": {
                      "GHO": "Men who have sex with men (MSM) with active syphilis (%)",
                      "YEAR": "2008",
                      "COUNTRY": "Morocco",
                      "REGION": "Eastern Mediterranean",
                      "PUBLISHSTATE": "Published"
                    },
             "Value": "23.3"
           },

```

- A partir de este objeto se crea esta clase para su serialización y utilización como objeto GSON, mostrandose una lista con todos los datos de los paises ordenados alfabéticamente, año y % de afectados.

```
#!java

 public class Syphilis implements Serializable {
        private String YEAR;
        private String COUNTRY;
        private String AGEGROUP;
        private String GHO;

        public String getYEAR() {
            return YEAR;
        }

        public void setYEAR(String YEAR) {
            this.YEAR = YEAR;
        }

        public String getCOUNTRY() {
            return COUNTRY;
        }

        public void setCOUNTRY(String COUNTRY) {
            this.COUNTRY = COUNTRY;
        }

        public String getAGEGROUP() {
            return AGEGROUP;
        }

        public void setAGEGROUP(String AGEGROUP) {
            this.AGEGROUP = AGEGROUP;
        }

        public String getGHO() {
            return GHO;
        }

        public void setGHO(String GHO) {
            this.GHO = GHO;
        }
    }

```
![Screenshot_1483483515.png](https://bitbucket.org/repo/Lq7x6a/images/3396112363-Screenshot_1483483515.png)


**3. Muertes a causa del tabaco (Objeto Json tipo "fact")**

```
#!json

"fact": [
           {
             "dim": {
                      "PUBLISHSTATE": "Published",
                      "SEX": "Female",
                      "REGION": "Africa",
                      "GHO": "Total NCD Deaths",
                      "COUNTRY": "Angola",
                      "YEAR": "2012"
                    },
             "Value": "36700"
           },
```

- A partir de este objeto se crea esta clase para su serialización y utilización como objeto GSON, mostrandose una lista con todos los datos de los paises ordenados alfabéticamente, año y muertes divididas por genero.

```
#!java
public class Smoke implements Serializable {
        private String YEAR;
        private String SEX;
        private String COUNTRY;
        private String REGION;
        private String GHO;

        public String getYEAR() {
            return YEAR;
        }

        public void setYEAR(String YEAR) {
            this.YEAR = YEAR;
        }

        public String getSEX() {
            return SEX;
        }

        public void setSEX(String SEX) {
            this.SEX = SEX;
        }

        public String getCOUNTRY() {
            return COUNTRY;
        }

        public void setCOUNTRY(String COUNTRY) {
            this.COUNTRY = COUNTRY;
        }

        public String getREGION() {
            return REGION;
        }

        public void setREGION(String REGION) {
            this.REGION = REGION;
        }

        public String getGHO() {
            return GHO;
        }

        public void setGHO(String GHO) {
            this.GHO = GHO;
        }
    }

```
![Screenshot_1483483693.png](https://bitbucket.org/repo/Lq7x6a/images/3583537485-Screenshot_1483483693.png)
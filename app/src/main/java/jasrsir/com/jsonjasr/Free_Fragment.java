package jasrsir.com.jsonjasr;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;


import jasrsir.com.jsonjasr.classes.AdapterOmsSmoke;
import jasrsir.com.jsonjasr.classes.AdapterOmsSy;
import jasrsir.com.jsonjasr.classes.AdapterOmsVi;
import jasrsir.com.jsonjasr.classes.Analizer;
import jasrsir.com.jsonjasr.classes.Comprobaciones;
import jasrsir.com.jsonjasr.classes.Oms;


public class Free_Fragment extends Fragment {

    static Context mContext;
    private ListView mlistComplete;
    private AdapterOmsSy adapterOmsSy;
    private AdapterOmsVi adapterOmsVi;
    private AdapterOmsSmoke adapterOmsSmoke;
    private ArrayList<Oms> listaSyfilis;
    private ArrayList<Oms> listaViolence;
    private ArrayList<Oms> listaSmoke;
    static Oms ooms;



    public Free_Fragment() {
        // Required empty public constructor
    }

    public static Free_Fragment newInstance(Context context, Oms oms) {
        Free_Fragment fragment = new Free_Fragment();
        Bundle args = new Bundle();
        mContext = context;
        ooms = oms;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       return inflater.inflate(R.layout.fragment_free, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mlistComplete = (ListView) view.findViewById(R.id.listComplete);
        listaSyfilis = new ArrayList<>();
        listaViolence = new ArrayList<>();
        listaSmoke = new ArrayList<>();

        if (Comprobaciones.isNetworkAvailable(mContext)){
            descargar(ooms);
        }else{
            Toast.makeText(mContext, "No tienes internet para realizar la operacion", Toast.LENGTH_LONG).show();
        }
        //adapterOmsSy = new AdapterOmsSy(mContext,mlistComplete)

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
        mlistComplete = null;
        adapterOmsSy = null;
        listaViolence = null;
        listaSyfilis = null;
        adapterOmsVi = null;
        adapterOmsSmoke = null;
        listaSmoke = null;
        ooms = null;
    }

    private void descargar(final Oms url){
        RequestQueue queue = Volley.newRequestQueue(mContext);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url.ruta,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (url.nameOms.startsWith("Men")) {
                    listaSyfilis = Analizer.analizarJsonSyphilis(response);
                    Collections.sort(listaSyfilis);
                    adapterOmsSy = new AdapterOmsSy(mContext, listaSyfilis);
                    mlistComplete.setDividerHeight(10);
                    mlistComplete.setAdapter(adapterOmsSy);
                } else  if (url.nameOms.startsWith("Int")){
                    listaViolence = Analizer.analizarJsonViolence(response);
                    Collections.sort(listaViolence);
                    adapterOmsVi = new AdapterOmsVi(mContext, listaViolence);
                    mlistComplete.setDividerHeight(10);
                    mlistComplete.setAdapter(adapterOmsVi);
                }else  if (url.nameOms.startsWith("Mue")){
                    listaSmoke = Analizer.analizarJsonSmoke(response);
                    Collections.sort(listaSmoke);
                    adapterOmsSmoke = new AdapterOmsSmoke(mContext, listaSmoke);
                    mlistComplete.setDividerHeight(10);
                    mlistComplete.setAdapter(adapterOmsSmoke);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(mContext, "No se pudo descargar el archivo", Toast.LENGTH_LONG).show();
            }
        });
        queue.add(request);
    }
}

package jasrsir.com.jsonjasr;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;

import android.net.Uri;
import android.os.Bundle;

import jasrsir.com.jsonjasr.classes.Oms;
import jasrsir.com.jsonjasr.classes.Weather;

public class Fragment_Activity extends Activity implements FreeInfo_Fragment.OnFragmentInteractionListener,CityList_Fragment.OnFragmentInteractionListener, SelectCity_Fragment.OnFragmentInteractionListener{

    private CityList_Fragment fragmentListCity;
    private SelectCity_Fragment fragmentOneCity;
    private EuroDollar_Fragment fragmentEuroDolar;
    private Free_Fragment fragmentFree;
    private FreeInfo_Fragment freeInfo_fragment;
    private String tipoEleccion;
    private CitySelected_Fragment citySelected_fragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tipoEleccion = getIntent().getExtras().getString("type");
        setContentView(R.layout.activity_fragment_);
        cargarFragment(tipoEleccion);
    }

    private void cargarFragment(String tipoEleccion) {
        switch (tipoEleccion) {
            case "weather":
                fragmentListCity = new CityList_Fragment().newInstance(this);
                getFragmentManager().beginTransaction().replace(R.id.activity_fragment_,fragmentListCity).commit();
                break;
            case "weatherfull":
                fragmentOneCity = new SelectCity_Fragment().newInstance(this);
                getFragmentManager().beginTransaction().replace(R.id.activity_fragment_,fragmentOneCity).commit();
                break;
            case "eurodolar":
                fragmentEuroDolar = new EuroDollar_Fragment().newInstance(this);
                getFragmentManager().beginTransaction().replace(R.id.activity_fragment_, fragmentEuroDolar).commit();
                break;
            case "free":
                freeInfo_fragment = new FreeInfo_Fragment().newInstance(this);
                getFragmentManager().beginTransaction().replace(R.id.activity_fragment_,freeInfo_fragment).commit();
                break;
        }

    }


    //TIEMPO DE CIUDAD SELECCIONADA.
    @Override
    public void onFragmentInteraction( int id,Weather ciudad) {
        citySelected_fragment = CitySelected_Fragment.newInstance(ciudad,this);
        getFragmentManager().beginTransaction().replace(id, citySelected_fragment).addToBackStack(null).commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }



    @Override
    public void onFragmentInteraction(int id, Oms ooms) {
        fragmentFree = Free_Fragment.newInstance(this, ooms);
        getFragmentManager().beginTransaction().replace(id,fragmentFree).addToBackStack(null).commit();
    }
}

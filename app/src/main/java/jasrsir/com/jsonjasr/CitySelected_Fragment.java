package jasrsir.com.jsonjasr;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.Date;

import jasrsir.com.jsonjasr.classes.Analizer;
import jasrsir.com.jsonjasr.classes.Comprobaciones;
import jasrsir.com.jsonjasr.classes.Weather;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CitySelected_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CitySelected_Fragment extends Fragment{

    private TextView txvNombre;
    private TextView txvFecha;
    private TextView txvPresion;
    private TextView txvHumedad;
    private TextView txvEstado;
    private TextView txvViento;
    private TextView txvTemperatura;
    private ImageView imgCielo;
    private static Weather ciudadSelect;
    static Context context;
    static ProgressDialog progreso;


    public CitySelected_Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.

     * @return A new instance of fragment CitySelected_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CitySelected_Fragment newInstance(Weather ciudad, Context contexto) {
        CitySelected_Fragment fragment = new CitySelected_Fragment();
        Bundle args = new Bundle();
        ciudadSelect = ciudad;
        context = contexto;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_city_selected, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progreso = new ProgressDialog(context);
        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progreso.setMessage("Mostrando tiempo actual. . .");
        progreso.setCancelable(false);
        progreso.show();
        init(view);
        if (Comprobaciones.isNetworkAvailable(context)){
            descargar(ciudadSelect.location);
        }else{
            progreso.dismiss();
            Toast.makeText(context, "No tienes internet para realizar la operacion", Toast.LENGTH_LONG).show();
        }
    }

    private void init(View container) {

        txvNombre = (TextView) container.findViewById(R.id.txvNombreCiudad);
        txvEstado = (TextView) container.findViewById(R.id.txvEstadocielo);
        txvFecha = (TextView) container.findViewById(R.id.txvFecha);
        txvHumedad = (TextView) container.findViewById(R.id.txvHumedad);
        txvPresion = (TextView) container.findViewById(R.id.txvPresion);
        txvTemperatura = (TextView) container.findViewById(R.id.txvtempe);
        txvViento = (TextView) container.findViewById(R.id.txvvitneo);
        imgCielo = (ImageView) container.findViewById(R.id.imvCielo);

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ciudadSelect = null;
    }

    private void descargar(String url){
        String urlfinal = String.format(getString(R.string.url_city_wacher),url);
        RequestQueue queue = Volley.newRequestQueue(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, urlfinal,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                ciudadSelect = Analizer.analizarJson(response);
                rellenarTiempo(ciudadSelect);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(context, "No se pudo descargar el archivo", Toast.LENGTH_LONG).show();
            }
        });
        queue.add(request);
    }

    private void rellenarTiempo(Weather ciudadSelect) {
        txvNombre.setText(ciudadSelect.name);
        txvEstado.setText(ciudadSelect.currentCondition.getDescription());
        txvFecha.setText(new Date().toString());
        txvHumedad.setText(ciudadSelect.temperature.getHumidity());
        txvPresion.setText(ciudadSelect.temperature.getPressure());
        txvTemperatura.setText(ciudadSelect.temperature.getTemp());
        txvViento.setText(ciudadSelect.wind.getSpeed());

        String recurso = getString(R.string.url_image_sky);
        String forRecurso = String.format(recurso, ciudadSelect.currentCondition.getIcon());
        Picasso.with(context).load(forRecurso).into(imgCielo);
        progreso.dismiss();
    }



}

package jasrsir.com.jsonjasr;

import android.app.Activity;
import android.content.Context;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import jasrsir.com.jsonjasr.classes.AdapterOmslist;
import jasrsir.com.jsonjasr.classes.Oms;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FreeInfo_Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FreeInfo_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FreeInfo_Fragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    static Context mContext;
    private ListView mListaOms;
    private AdapterOmslist adapterOmslist;
    private List<Oms> list;

    public FreeInfo_Fragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static FreeInfo_Fragment newInstance(Context context) {
        FreeInfo_Fragment fragment = new FreeInfo_Fragment();
        Bundle args = new Bundle();
        mContext = context;

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_free_info, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        list = new ArrayList<>();
        list.add(new Oms("Intimate partner violence prevalence among ever partnered women (%)","http://apps.who.int/gho/athena/data/GHO/RHR_IPV.json?profile=simple&filter=COUNTRY:-;WHOINCOMEREGION:*"));
        list.add(new Oms("Men who have sex with men (MSM) with active syphilis (%)","http://apps.who.int/gho/athena/data/GHO/PercposMSM.json?profile=simple&filter=COUNTRY:*"));
        list.add(new Oms("Muertes a causa del tabaco","http://apps.who.int/gho/athena/data/GHO/NCD_DTH_TOT.json?profile=simple&filter=COUNTRY:*;SEX:*"));
        mListaOms = (ListView) view.findViewById(R.id.listOMS);
        adapterOmslist = new AdapterOmslist(mContext,list);
        mListaOms.setDividerHeight(15);

        mListaOms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mListener.onFragmentInteraction(getId(), (Oms) adapterView.getItemAtPosition(i));
            }
        });
        mListaOms.setAdapter(adapterOmslist);
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(int id,Oms ooms);
    }
}

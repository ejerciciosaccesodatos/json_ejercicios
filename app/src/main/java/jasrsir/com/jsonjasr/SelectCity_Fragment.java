package jasrsir.com.jsonjasr;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;

import jasrsir.com.jsonjasr.classes.AdapterDialyWeather;
import jasrsir.com.jsonjasr.classes.Comprobaciones;
import jasrsir.com.jsonjasr.classes.StreamIO;
import jasrsir.com.jsonjasr.classes.Weather;

import static jasrsir.com.jsonjasr.classes.Analizer.analizarJsonDialy;


public class SelectCity_Fragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    static Context mContext;
    private EditText mEdtCiudad;
    private Button mBtnTiempo;
    private ListView mLtvTiempo;
    private AdapterDialyWeather mAdapterDiario;
    static ProgressDialog progreso;


    public SelectCity_Fragment() {
        // Required empty public constructor
    }

    public static SelectCity_Fragment newInstance(Context context) {
        SelectCity_Fragment fragment = new SelectCity_Fragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        mContext = context;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_select_city, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBtnTiempo = (Button) view.findViewById(R.id.btnTIempo);
        mEdtCiudad = (EditText) view.findViewById(R.id.edtNombreCiudad);
        mLtvTiempo = (ListView) view.findViewById(R.id.listTiempo);
        mBtnTiempo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!Comprobaciones.isNetworkAvailable(mContext)){
                    Toast.makeText(mContext, "No hay conexion a internet", Toast.LENGTH_LONG).show();
                }else if(mEdtCiudad.getText().toString().isEmpty()){
                    mEdtCiudad.setError("Este campo no puede estar vacio");
                }else {
                    if(!Comprobaciones.sdAvaiable())
                        Toast.makeText(mContext, "No hay SD los ficheros xml y json no seran guardados", Toast.LENGTH_LONG).show();

                    progreso = new ProgressDialog(mContext);
                    progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progreso.setMessage("descargando archivos y mostrando tiempo. . .");
                    progreso.setCancelable(false);
                    progreso.show();
                    descargar(mEdtCiudad.getText().toString());
                    progreso.dismiss();
                }
            }
        });
    }
    private void descargar(final String s) {

        String requestXml = String.format(getString(R.string.url_list_wacher),s, "xml");
        String requestJson = String.format(getString(R.string.url_list_wacher),s, "json");
        RequestQueue cola = Volley.newRequestQueue(mContext);

        StringRequest xml = new StringRequest(Request.Method.GET, requestXml, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                StreamIO stream = new StreamIO(Environment.getExternalStorageDirectory().getAbsolutePath());
                stream.writeFile("predicciones" + s +".xml", response, false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(mContext, "Error en descarga del xml", Toast.LENGTH_LONG).show();
            }
        });

        JsonObjectRequest json = new JsonObjectRequest(Request.Method.GET, requestJson,
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                StreamIO stream = new StreamIO(Environment.getExternalStorageDirectory().getAbsolutePath());
                stream.writeFile("predicciones" + s +".json", response.toString(), false);
                ArrayList<Weather> tiempo = analizarJsonDialy(response);
                mAdapterDiario = new AdapterDialyWeather(mContext, tiempo);
                mLtvTiempo.setAdapter(mAdapterDiario);
                mLtvTiempo.setDividerHeight(12);
                mLtvTiempo.setVisibility(View.VISIBLE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(mContext, "Error en descarga del json", Toast.LENGTH_LONG).show();
            }
        });
        cola.add(json);
        cola.add(xml);
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mAdapterDiario = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

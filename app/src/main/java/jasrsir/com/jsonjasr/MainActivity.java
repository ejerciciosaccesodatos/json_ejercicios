package jasrsir.com.jsonjasr;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.Map;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnWeather:
                startActivity(new Intent(MainActivity.this, Fragment_Activity.class).putExtra("type","weather"));
                break;
            case R.id.btnWeatherFull:
                startActivity(new Intent(MainActivity.this, Fragment_Activity.class).putExtra("type","weatherfull"));
                break;
            case R.id.btnEuroDolar:
                startActivity(new Intent(MainActivity.this, Fragment_Activity.class).putExtra("type","eurodolar"));
                break;
            case R.id.btnFree:
                startActivity(new Intent(MainActivity.this,Fragment_Activity.class).putExtra("type","free"));
                break;
        }
    }
}

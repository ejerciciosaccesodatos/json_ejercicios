package jasrsir.com.jsonjasr.classes;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import jasrsir.com.jsonjasr.R;

/**
 * Created by jasrsir on 8/12/16.
 */

public class AdapterOmsVi extends ArrayAdapter {
    private Context mContext;

    //Class to content a view
    class CardViewHolder {
        TextView mTxvCiudad;
        TextView mTxvValor;
        TextView mTxvAnio;
        TextView mTxvGroup;
        TextView mTxvSex;

    }
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //Variables vista y contenedor de la vista
        View item = convertView;
        CardViewHolder cardViewHolder;

        //Preguntamos si la vista es nula, si es nula ya lo inicializamos
        if (item == null) {
            //Creamos un objeto inflater que inicializamos al LayoutInflater del contexto
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // Inflar la vista, crear en memoria el objeto VIew   que contiene los widgets del XML
            item = inflater.inflate(R.layout.card_oms_vi, null);
            cardViewHolder = new CardViewHolder();

            // Asignamos a las vbariables los widgets mediante el metodo findViewById
            cardViewHolder.mTxvCiudad = (TextView) item.findViewById(R.id.txvCountryvi);
            cardViewHolder.mTxvAnio = (TextView) item.findViewById(R.id.txvYearvi);
            cardViewHolder.mTxvValor = (TextView) item.findViewById(R.id.txvValuevi);
            cardViewHolder.mTxvGroup = (TextView) item.findViewById(R.id.txvAegroupvi);
            cardViewHolder.mTxvSex = (TextView) item.findViewById(R.id.txvSexovi);
            item.setTag(cardViewHolder);
        } else
            cardViewHolder = (CardViewHolder) item.getTag();
        // Asignamos los datos del adapter a los widgets
        cardViewHolder.mTxvCiudad.setText(((Oms)getItem(position)).violence.getWHOINCOMEREGION());
        cardViewHolder.mTxvAnio.setText(((Oms)getItem(position)).violence.getYEAR());
        cardViewHolder.mTxvValor.setText(((Oms)getItem(position)).Value);
        cardViewHolder.mTxvGroup.setText(((Oms)getItem(position)).violence.getAGEGROUP());
        cardViewHolder.mTxvSex.setText(((Oms)getItem(position)).violence.getSEX());

        return item;
    }

    public AdapterOmsVi(Context context, List<Oms> lista) {
        super(context, R.layout.card_oms_vi ,lista);
        this.mContext = context;
    }

}

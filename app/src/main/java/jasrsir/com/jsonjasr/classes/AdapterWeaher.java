package jasrsir.com.jsonjasr.classes;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import jasrsir.com.jsonjasr.R;

/**
 * Created by jasrsir on 8/12/16.
 */

public class AdapterWeaher extends ArrayAdapter {
    private Context mContext;

    //Class to content a view
    class CardViewHolder {
        TextView mTxvCiudad;

    }
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //Variables vista y contenedor de la vista
        View item = convertView;
        CardViewHolder cardViewHolder;

        //Preguntamos si la vista es nula, si es nula ya lo inicializamos
        if (item == null) {
            //Creamos un objeto inflater que inicializamos al LayoutInflater del contexto
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // Inflar la vista, crear en memoria el objeto VIew   que contiene los widgets del XML
            item = inflater.inflate(R.layout.card_city, null);
            cardViewHolder = new CardViewHolder();

            // Asignamos a las vbariables los widgets mediante el metodo findViewById
            cardViewHolder.mTxvCiudad = (TextView) item.findViewById(R.id.txvNameContact);

            item.setTag(cardViewHolder);
        } else
            cardViewHolder = (CardViewHolder) item.getTag();
        // Asignamos los datos del adapter a los widgets
        cardViewHolder.mTxvCiudad.setText(((Weather)getItem(position)).name);
        return item;
    }

    public AdapterWeaher(Context context, List<Weather> tiempo) {
        super(context, R.layout.card_city ,tiempo);
        this.mContext = context;
    }

}

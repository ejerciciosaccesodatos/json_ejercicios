package jasrsir.com.jsonjasr.classes;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;

import static android.content.Context.CONNECTIVITY_SERVICE;

/**
 * Created by amador on 25/12/16.
 */

public class Comprobaciones {

    public static boolean isNetworkAvailable(Context context){
        boolean result = false;
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            result = true;

        return result;

    }

    public static boolean sdAvaiable(){

        String state = Environment.getExternalStorageState();
        return state.equals(Environment.MEDIA_MOUNTED);
    }
}

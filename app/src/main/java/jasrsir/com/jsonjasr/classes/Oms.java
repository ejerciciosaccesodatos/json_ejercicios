package jasrsir.com.jsonjasr.classes;

import java.io.Serializable;

/**
 * Created by jasrsir on 3/01/17.
 */

public class Oms implements Serializable, Comparable {
    public String Value;
    public Syphilis syphilis = new Syphilis();
    public Violence violence = new Violence();
    public Smoke smoke = new Smoke();
    public String nameOms;
    public String ruta;

    public Oms(String nameOms, String ruta) {
        this.nameOms = nameOms;
        this.ruta = ruta;
    }

    @Override
    public int compareTo(Object o) {
        if (((Oms) o).syphilis != null) {
            if (this.syphilis.getCOUNTRY().compareTo(((Oms) o).syphilis.getCOUNTRY())==0)
                return this.syphilis.getYEAR().compareTo(((Oms) o).syphilis.getYEAR());
            else
                return this.syphilis.getCOUNTRY().compareTo(((Oms) o).syphilis.getCOUNTRY());
        } else if (((Oms) o).violence != null) {
            if (this.violence.getWHOINCOMEREGION().compareTo(((Oms) o).violence.getWHOINCOMEREGION()) == 0)
                return this.violence.getAGEGROUP().compareTo(((Oms) o).violence.getAGEGROUP());
            else
                return this.violence.getWHOINCOMEREGION().compareTo(((Oms) o).violence.getWHOINCOMEREGION());
        } else {
            if (this.smoke.getCOUNTRY().compareTo(((Oms) o).smoke.getCOUNTRY())==0)
                return this.smoke.getYEAR().compareTo(((Oms) o).smoke.getYEAR());
            else
                return this.smoke.getCOUNTRY().compareTo(((Oms) o).smoke.getCOUNTRY());
        }

    }

    public class Violence implements Serializable {
        private String YEAR;
        private String SEX;
        private String AGEGROUP;
        private String WHOINCOMEREGION;
        private String GBDREGION;
        private String GHO;

        public String getYEAR() {
            return YEAR;
        }

        public void setYEAR(String YEAR) {
            this.YEAR = YEAR;
        }

        public String getSEX() {
            return SEX;
        }

        public void setSEX(String SEX) {
            this.SEX = SEX;
        }

        public String getAGEGROUP() {
            return AGEGROUP;
        }

        public void setAGEGROUP(String AGEGROUP) {
            this.AGEGROUP = AGEGROUP;
        }

        public String getWHOINCOMEREGION() {
            return WHOINCOMEREGION;
        }

        public void setWHOINCOMEREGION(String WHOINCOMEREGION) {
            this.WHOINCOMEREGION = WHOINCOMEREGION;
        }

        public String getGBDREGION() {
            return GBDREGION;
        }

        public void setGBDREGION(String GBDREGION) {
            this.GBDREGION = GBDREGION;
        }

        public String getGHO() {
            return GHO;
        }

        public void setGHO(String GHO) {
            this.GHO = GHO;
        }
    }

    public class Syphilis implements Serializable {
        private String YEAR;
        private String COUNTRY;
        private String AGEGROUP;
        private String GHO;

        public String getYEAR() {
            return YEAR;
        }

        public void setYEAR(String YEAR) {
            this.YEAR = YEAR;
        }

        public String getCOUNTRY() {
            return COUNTRY;
        }

        public void setCOUNTRY(String COUNTRY) {
            this.COUNTRY = COUNTRY;
        }

        public String getAGEGROUP() {
            return AGEGROUP;
        }

        public void setAGEGROUP(String AGEGROUP) {
            this.AGEGROUP = AGEGROUP;
        }

        public String getGHO() {
            return GHO;
        }

        public void setGHO(String GHO) {
            this.GHO = GHO;
        }
    }

    public class Smoke implements Serializable {
        private String YEAR;
        private String SEX;
        private String COUNTRY;
        private String REGION;
        private String GHO;

        public String getYEAR() {
            return YEAR;
        }

        public void setYEAR(String YEAR) {
            this.YEAR = YEAR;
        }

        public String getSEX() {
            return SEX;
        }

        public void setSEX(String SEX) {
            this.SEX = SEX;
        }

        public String getCOUNTRY() {
            return COUNTRY;
        }

        public void setCOUNTRY(String COUNTRY) {
            this.COUNTRY = COUNTRY;
        }

        public String getREGION() {
            return REGION;
        }

        public void setREGION(String REGION) {
            this.REGION = REGION;
        }

        public String getGHO() {
            return GHO;
        }

        public void setGHO(String GHO) {
            this.GHO = GHO;
        }
    }

}

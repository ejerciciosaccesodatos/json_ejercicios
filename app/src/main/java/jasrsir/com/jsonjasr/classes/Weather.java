package jasrsir.com.jsonjasr.classes;

import java.io.Serializable;

/**
 * Created by jasrsir on 28/12/16.
 */

public class Weather implements Serializable {

    public Condition currentCondition = new Condition();
    public Temperature temperature = new Temperature();
    public Wind wind = new Wind();
    public String name;
    public String dt;
    public String location;

    public Weather(String location, String name) {
        this.location = location;
        this.name = name;
    }

    public Weather() {
    }

    public class Condition implements Serializable {
        private int id;
        private String main;
        private String description;
        private String icon;


        public int getId() {
            return id;
        }
        public void setId(int id) {
            this.id = id;
        }
        public String getMain() {
            return main;
        }
        public void setMain(String main) {
            this.main = main;
        }
        public String getDescription() {
            return description;
        }
        public void setDescription(String description) {
            this.description = description;
        }
        public String getIcon() {
            return icon;
        }
        public void setIcon(String icon) {
            this.icon = icon;
        }

    }

    public  class Temperature implements Serializable {
        private String temp;
        private String min;
        private String max;

        private String pressure;
        private String humidity;

        public String getTemp() {
            return temp;
        }
        public void setTemp(String temp) {
            this.temp = temp;
        }
        public String getMin() {
            return min;
        }
        public void setMin(String min) {
            this.min = min;
        }
        public String getMax() {
            return max;
        }
        public void setMax(String max) {
            this.max = max;
        }
        public String getPressure() {
            return pressure;
        }
        public void setPressure(String pressure) {
            this.pressure = pressure;
        }
        public String getHumidity() {
            return humidity;
        }
        public void setHumidity(String humidity) {
            this.humidity = humidity;
        }


    }

    public  class Wind  implements Serializable{
        private String speed;
        private String deg;
        public String getSpeed() {
            return speed;
        }
        public void setSpeed(String speed) {
            this.speed = speed;
        }
        public String getDeg() {
            return deg;
        }
        public void setDeg(String deg) {
            this.deg = deg;
        }


    }




}
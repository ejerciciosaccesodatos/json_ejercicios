package jasrsir.com.jsonjasr.classes;



import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Class Analizer to JSON Exercices
 */

public class Analizer {

    public static Weather analizarJson(JSONObject obj){

        Weather estadoCiudad = new Weather();

        try {

            estadoCiudad.name = obj.getString("name");
            JSONArray weather = obj.getJSONArray("weather");
            estadoCiudad.currentCondition.setIcon((weather.getJSONObject(0).getString("icon")));
            estadoCiudad.currentCondition.setDescription(weather.getJSONObject(0).getString("description"));
            estadoCiudad.temperature.setHumidity("Humedad: "+obj.getJSONObject("main").getString("humidity")+"%");
            double temperatura = Double.parseDouble(obj.getJSONObject("main").getString("temp"));
            temperatura -= 273.15;
            estadoCiudad.temperature.setTemp(String.format("%.2f",temperatura)+" Cº");
            estadoCiudad.temperature.setPressure("Presión: "+obj.getJSONObject("main").getString("pressure")+" hpa");
            estadoCiudad.wind.setSpeed("Viento: "+obj.getJSONObject("wind").getString("speed")+" m/s");


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return estadoCiudad;

    }

    public static  ArrayList<Weather> analizarJsonDialy(JSONObject response) {

        ArrayList<Weather> listaTiempo;

        Type type = new TypeToken<ArrayList<Weather>>(){}.getType();
        Gson gson = new Gson();
        try {

            JSONArray jsonArray = response.getJSONArray("list");
            listaTiempo = gson.fromJson(String.valueOf(jsonArray), type);

            for (int i = 0; i < jsonArray.length(); i++){

                Weather.Condition c = gson.fromJson(jsonArray.getJSONObject(i).getJSONArray("weather").getJSONObject(0).toString(), Weather.Condition.class);
                Weather.Temperature t = gson.fromJson(jsonArray.getJSONObject(i).getJSONObject("temp").toString(), Weather.Temperature.class);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

                listaTiempo.get(i).dt =simpleDateFormat.format(new Date(Long.parseLong(jsonArray.getJSONObject(i).getString("dt")) * 1000));
                listaTiempo.get(i).currentCondition = c;
                listaTiempo.get(i).name = response.getJSONObject("city").getString("name");
                listaTiempo.get(i).temperature.setTemp(
                        "Máximas de " + t.getMax()+" Cº\n" +
                        "Mínimas de " + t.getMin()+" Cº\n");
                listaTiempo.get(i).temperature.setHumidity("Humedad: "+jsonArray.getJSONObject(i).getString("humidity")+"%");
                listaTiempo.get(i).temperature.setPressure("Presión: "+ jsonArray.getJSONObject(i).getString("pressure")+" hpa");
                listaTiempo.get(i).wind.setSpeed("Viento: "+jsonArray.getJSONObject(i).getString("speed")+" m/s");
            }

            return listaTiempo;

        } catch (JSONException e) {

        }
        return null;

    }

    public static  ArrayList<Oms> analizarJsonSyphilis(JSONObject response) {

        ArrayList<Oms> listasyphilis;

        Type type = new TypeToken<ArrayList<Oms>>(){}.getType();
        Gson gson = new Gson();
        try {

            JSONArray jsonArray = response.getJSONArray("fact");
            listasyphilis = gson.fromJson(String.valueOf(jsonArray), type);

            for (int i = 0; i < jsonArray.length(); i++){

                Oms.Syphilis syphilis = gson.fromJson(jsonArray.getJSONObject(i).getJSONObject("dim").toString(), Oms.Syphilis.class);

                listasyphilis.get(i).Value ="Valor : " + jsonArray.getJSONObject(i).getString("Value") + " %";
                listasyphilis.get(i).syphilis = syphilis;
                listasyphilis.get(i).syphilis.setYEAR("Año: " + syphilis.getYEAR());
                listasyphilis.get(i).syphilis.setCOUNTRY("País: "+syphilis.getCOUNTRY());
            }

            return listasyphilis;

        } catch (JSONException e) {

        }
        return null;

    }

    public static  ArrayList<Oms> analizarJsonViolence(JSONObject response) {

        ArrayList<Oms> listaviolence;

        Type type = new TypeToken<ArrayList<Oms>>(){}.getType();
        Gson gson = new Gson();
        try {

            JSONArray jsonArray = response.getJSONArray("fact");
            listaviolence = gson.fromJson(String.valueOf(jsonArray), type);

            for (int i = 0; i < jsonArray.length(); i++){

                Oms.Violence violence = gson.fromJson(jsonArray.getJSONObject(i).getJSONObject("dim").toString(), Oms.Violence.class);

                listaviolence.get(i).Value ="Valor : " + jsonArray.getJSONObject(i).getString("Value") + " % ,[min,max]";
                listaviolence.get(i).violence = violence;
                listaviolence.get(i).violence.setYEAR("Año: " + violence.getYEAR());
                listaviolence.get(i).violence.setWHOINCOMEREGION(violence.getWHOINCOMEREGION());
                listaviolence.get(i).violence.setSEX("Sexo: "+violence.getSEX());
                listaviolence.get(i).violence.setAGEGROUP("Grupo Edad: "+violence.getAGEGROUP());
            }

            return listaviolence;

        } catch (JSONException e) {

        }
        return null;
    }

    public static  ArrayList<Oms> analizarJsonSmoke(JSONObject response) {

        ArrayList<Oms> listasmoke;

        Type type = new TypeToken<ArrayList<Oms>>(){}.getType();
        Gson gson = new Gson();
        try {

            JSONArray jsonArray = response.getJSONArray("fact");
            listasmoke = gson.fromJson(String.valueOf(jsonArray), type);

            for (int i = 0; i < jsonArray.length(); i++){

                Oms.Smoke smoke = gson.fromJson(jsonArray.getJSONObject(i).getJSONObject("dim").toString(), Oms.Smoke.class);

                listasmoke.get(i).Value ="Valor : " + jsonArray.getJSONObject(i).getString("Value") + " DEADS";
                listasmoke.get(i).smoke = smoke;
                listasmoke.get(i).smoke.setYEAR("Año: " + smoke.getYEAR());
                listasmoke.get(i).smoke.setCOUNTRY("País: "+smoke.getCOUNTRY());
                listasmoke.get(i).smoke.setSEX("Sexo: "+smoke.getSEX());
                listasmoke.get(i).smoke.setREGION("Region: "+smoke.getREGION());
            }

            return listasmoke;

        } catch (JSONException e) {

        }
        return null;
    }
}

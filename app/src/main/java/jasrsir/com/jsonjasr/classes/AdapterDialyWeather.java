package jasrsir.com.jsonjasr.classes;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import jasrsir.com.jsonjasr.R;

/**
 * Created by jasrsir on 2/01/17.
 */

public class AdapterDialyWeather extends ArrayAdapter {


    Context mcontext;

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CardViewHolderDialy holderDialy;
        View v = convertView;

        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.card_dialy,null);

            holderDialy = new CardViewHolderDialy();
            holderDialy.txvNombre = (TextView) v.findViewById(R.id.txvNombreCiudadd);
            holderDialy.txvEstado = (TextView) v.findViewById(R.id.txvEstadocielod);
            holderDialy.txvFecha = (TextView) v.findViewById(R.id.txvFechad);
            holderDialy.txvHumedad = (TextView) v.findViewById(R.id.txvHumedadd);
            holderDialy.txvPresion = (TextView) v.findViewById(R.id.txvPresiond);
            holderDialy.txvTemperatura = (TextView) v.findViewById(R.id.txvtemped);
            holderDialy.txvViento = (TextView) v.findViewById(R.id.txvvitneod);
            holderDialy.imgCielo = (ImageView) v.findViewById(R.id.imvCielod);

            v.setTag(holderDialy);
        } else {

            holderDialy = (CardViewHolderDialy) v.getTag();

          }
        String recurso = mcontext.getString(R.string.url_image_sky);
        String forRecurso = String.format(recurso, ((Weather)getItem(position)).currentCondition.getIcon());
        Picasso.with(mcontext).load(forRecurso).into(holderDialy.imgCielo);
        holderDialy.txvEstado.setText(((Weather)getItem(position)).currentCondition.getDescription());
        holderDialy.txvNombre.setText(((Weather)getItem(position)).name);
        holderDialy.txvFecha.setText(((Weather)getItem(position)).dt);
        holderDialy.txvHumedad.setText(((Weather)getItem(position)).temperature.getHumidity());
        holderDialy.txvPresion.setText(((Weather)getItem(position)).temperature.getPressure());
        holderDialy.txvTemperatura.setText(((Weather)getItem(position)).temperature.getTemp());
        holderDialy.txvViento.setText(((Weather)getItem(position)).wind.getSpeed());


        return v;

    }

    public AdapterDialyWeather(Context context, List<Weather> lista) {
        super(context, R.layout.card_dialy, lista);
        mcontext = context;
    }

    public class CardViewHolderDialy {
         TextView txvNombre;
         TextView txvFecha;
         TextView txvPresion;
         TextView txvHumedad;
         TextView txvEstado;
         TextView txvViento;
         TextView txvTemperatura;
         ImageView imgCielo;
    }
}

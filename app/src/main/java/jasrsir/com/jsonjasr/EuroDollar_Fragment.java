package jasrsir.com.jsonjasr;


import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class EuroDollar_Fragment extends Fragment {

    //Creacion de variables necesarias
    private EditText dolares;
    private EditText euros;
     static Context mContext;
    private RadioButton dolarEuro;
    private static final String URL = "http://api.fixer.io/latest";
    private RadioButton euroDolar;
    private Button convertir;
    TextView resulado;
    private Toast notificacion;
    private double ratio;

    public EuroDollar_Fragment() {
        // Required empty public constructor
    }


    public static EuroDollar_Fragment newInstance(Context context) {
        EuroDollar_Fragment fragment = new EuroDollar_Fragment();
        Bundle args = new Bundle();
        mContext = context;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //inicializamos las variables con sus componentes correspondientes.
        View vista = inflater.inflate(R.layout.fragment_euro_dollar, container, false);
        dolares = (EditText) vista.findViewById(R.id.txtDolar);
        euros = (EditText) vista.findViewById(R.id.txtEuro);
        dolarEuro = (RadioButton) vista.findViewById(R.id.rbtnDoltoEur);
        euroDolar = (RadioButton) vista.findViewById(R.id.rbtnEuroDolar);
        convertir = (Button) vista.findViewById(R.id.button);
        convertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                        if (dolarEuro.isChecked()){
                            String valor = dolares.getText().toString();
                            if (valor.isEmpty() || valor == ".")
                                valor = "0";
                            else
                                euros.setText( String.valueOf( Double.parseDouble(valor)/ratio));
                        }
                        else {
                            String valor = euros.getText().toString();
                            if (valor.isEmpty() || valor == ".")
                                valor = "0";
                            else
                                dolares.setText( String.valueOf(ratio* Double.parseDouble(valor)));
                        }

                } catch (NumberFormatException exception){
                    notificacion = Toast.makeText(mContext, "Error al convertir el número", Toast.LENGTH_LONG);
                    notificacion.show();
                }
            }});
        resulado = (TextView) vista.findViewById(R.id.textView);

        return vista;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(!hayInternet())
            Toast.makeText(mContext, "No hay una conexion a internet para comprobar los ratios", Toast.LENGTH_LONG).show();
        else
            conexion();

    }

    private boolean hayInternet() {
        boolean result = false;
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            result = true;

        return result;

    }

    //Tarea para descargar los datos
    private void conexion(){


        RequestQueue queue = Volley.newRequestQueue(mContext);
        final ProgressDialog progress = new ProgressDialog(mContext);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                URL, null, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

                try {

                    JSONObject ratios = response.getJSONObject("rates"); //Extrae el objeto JSON con los ratios
                    String fecha = response.getString("date"); //Extraigo la fecha
                    String valor = ratios.getString("USD"); //Es te es el ratio que buscamos
                    resulado.setText("Fecha: "+fecha+" 1 Euro equivale a "+valor+" Dolares"); //Informo
                    ratio = Double.parseDouble(valor); //Convierto


                } catch (JSONException e) {

                    //Si se produjo un error en la lectura
                    Toast.makeText(mContext, "Se produjo un error en la lectura del archivo", Toast.LENGTH_LONG).show();

                } catch (NumberFormatException e){

                    //Por si hay un error en la conversion que nunca se sabe
                    Toast.makeText(mContext, "Los datos del archivo provocaron un error de conversión", Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //Si hay un error
                Toast.makeText(mContext, "Se produjo el siguiente error "+error.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

        queue.add(jsonObjectRequest); //A la cola

    }
   
}

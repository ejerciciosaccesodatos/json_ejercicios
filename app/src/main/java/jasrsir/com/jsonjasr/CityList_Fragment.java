package jasrsir.com.jsonjasr;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import jasrsir.com.jsonjasr.classes.AdapterWeaher;
import jasrsir.com.jsonjasr.classes.Weather;


public class CityList_Fragment extends ListFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ArrayList<Weather> arrayMap;
    static Context mContext;
    private AdapterWeaher adapterWeaher;

    private OnFragmentInteractionListener mListener;

    public CityList_Fragment() {
        // Required empty public constructor
    }


    public static CityList_Fragment newInstance(Context context) {
        CityList_Fragment fragment = new CityList_Fragment();
        Bundle args = new Bundle();
        mContext = context;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        arrayMap = new ArrayList<>();
        arrayMap.add(new Weather("2514256","Málaga"));
        arrayMap.add(new Weather("3117735","Madrid"));
        arrayMap.add(new Weather("3128760","Barcelona"));
        arrayMap.add(new Weather("2509954","Valencia"));
        arrayMap.add(new Weather("2510911","Sevilla"));
        adapterWeaher = new AdapterWeaher(mContext,arrayMap);
        return inflater.inflate(R.layout.fragment_city_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getListView().setDividerHeight(10);

        getListView().setAdapter(adapterWeaher);
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        arrayMap = null;
        adapterWeaher = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {

        void onFragmentInteraction( int id, Weather ciudad);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        mListener.onFragmentInteraction(this.getId(),(Weather)l.getItemAtPosition(position));

    }


}
